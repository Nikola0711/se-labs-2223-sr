import sys

user1 = input("Upisi svoje ime?")
user2 = input("Upisi svoje ime?")
user1_answer = input("%s, zelis li izabrati kamen, papir ili skare?" % user1)
user2_answer = input("%s, zelis li izabrati kamen, papir ili skare?" % user2)

def compare(u1, u2):
    if u1 == u2:
        return("Izjednaceno!")
    elif u1 == 'kamen':
        if u2 == 'skare':
            return("Kamen pobjeduje!")
        else:
            return("Papir pobjeduje!")
    elif u1 == 'skare':
        if u2 == 'papir':
            return("Skare pobjeduju!")
        else:
            return("Kamen pobjeduje!")
    elif u1 == 'papir':
        if u2 == 'kamen':
            return("Papir pobjeduje!")
        else:
            return("Skare pobjeduju!")
    else:
        return("Krivi unos! Niste izabrali kamen, papir ili skare, pokusajte ponovno.")
        sys.exit()

print(compare(user1_answer, user2_answer))