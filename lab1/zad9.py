import random

rd = random.randint(1,9)
guess = 0
c = 0
while guess != rd and guess != "izlaz":
    guess = input("Unesite pokusaj izmedu 1 i 9:")

    if guess == "izlaz":
        break

    guess = int(guess)
    c += 1

    if guess < rd:
        print("Nisko")
    elif guess > rd:
        print("Visoko")
    else:
        print("Tocno!")
        print("Trebalo ti je samo", c, "pokusaja!")
input()