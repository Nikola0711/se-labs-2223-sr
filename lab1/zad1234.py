#### Exercise 1
"""Input the comma-separated integers and transform them into a list of integers.
Print the number of even ints in the given list. Note: the % "mod" operator computes the remainder, e.g. 5 % 2 is 1.
Use a function for counting evens.

        count_evens([2, 1, 2, 3, 4]) → 3
        count_evens([2, 2, 0]) → 3
        count_evens([1, 3, 5]) → 0"""

"""my_str = '1,2,3,4'

my_list = [int(item) for item in my_str.split(',') if item.isdigit()] #comma-sep int into list of ints
print(my_list)
even_count=0
for mod in my_list:
 if mod % 2 == 0:
        even_count += 1

print(even_count)"""


#### Exercise 2
"""Input the comma-separated integers and transform them into a list of integers.
Return the "centered" average of an list of ints, which we'll say is the mean
average of the values, except ignoring the largest and smallest values in the
list. If there are multiple copies of the smallest value, ignore just one copy,
and likewise for the largest value. Use int division to produce the final
average. You may assume that the list is length 3 or more.

        centered_average([1, 2, 3, 4, 100]) → 3
        centered_average([1, 1, 5, 5, 10, 8, 7]) → 5
        centered_average([-10, -4, -2, -4, -2, 0]) → -3"""

"""nums =[1, 2, 3, 4, 100]
def solution(input):

    average = 0

    minimum = min(input)
    maximum = max(input)

    input.pop(input.index(minimum))
    input.pop(input.index(maximum))

    average =  round(sum(input) / len(input))

    return average

print(solution(nums))"""


#### Exercise 3
"""Input the comma-separated integers and transform them into a list of integers.
Given a list of ints, return True if the list contains a 2 next to a 2 somewhere."""

nums =[1, 2, 2, 4, 100]

for i in nums:
    for j in nums:
        if i==2 and j==2:
            print("true")
        else:
            print("false")

#### Exercise 4
"""Input a string and pass it to the function. The function returns a tuple of
three values: a count of uppercase letters, a count of lowercase letters and a
count of numbers.

        char_types_count("asdf98CXX21grrr") → (3,8,4)"""


def char_types_count(s):
    d={"UPPER_CASE":0, "LOWER_CASE":0, "DIGIT":0}
    for c in s:
        if c.isupper():
           d["UPPER_CASE"]+=1
        elif c.islower():
           d["LOWER_CASE"]+=1
        elif c.isdigit():
            d["DIGIT"]+=1
        else:
           pass
    print ("Original String : ", s)
    print ("No. of Upper case characters : ", d["UPPER_CASE"])
    print ("No. of Lower case Characters : ", d["LOWER_CASE"])
    print ("No. of digits : ", d["DIGIT"])

char_types_count("asdf98CXX21grrr")
